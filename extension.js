/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const GObject = imports.gi.GObject;
const GLib = imports.gi.GLib;
const Main = imports.ui.main;

const canDisconnect = imports.system.version > 16390;
const gsVersion = imports.misc.config.PACKAGE_VERSION >= '40' ? 'GS40' : 'GS38';

function disconnectAllHandlers() {
    let id;
    while (id = GObject.signal_handler_find(global.display, { signalId: 'overlay-key' }))
        global.display.disconnect(id);
}

const onOverlayKeyPressedOriginal = {
    // GS 3.38-
    // See _initializeUI (main.js)
    'GS38': function() {
        if (!Main._a11ySettings.get_boolean(Main.STICKY_KEYS_ENABLE))
            Main.overview.toggle();
    },

    // GS 3.40+
    // See ControlsManager (overviewControls.js)
    'GS40': (function() {
        if (this._a11ySettings.get_boolean('stickykeys-enable'))
            return;

        const { initialState, finalState, transitioning } =
            this._stateAdjustment.getStateTransitionParams();

        const time = GLib.get_monotonic_time() / 1000;
        const timeDiff = time - this._lastOverlayKeyTime;
        this._lastOverlayKeyTime = time;

        const shouldShift = imports.gi.St.Settings.get().enable_animations
            ? transitioning && finalState > initialState
            : Main.overview.visible && timeDiff < imports.ui.overview.ANIMATION_TIME;

        if (shouldShift)
            this._shiftState(imports.gi.Meta.MotionDirection.UP);
        else
            Main.overview.toggle();
    }).bind(
        Main.overview._overview.controls
    ),
};

const onOverlayKeyPressedCustom = {
    // GS 3.38-
    'GS38': function() {
        if (Main.overview.visible)
            Main.overview.hide();
        else
            Main.overview.viewSelector.showApps(); 
    },

    // GS 3.40+
    'GS40': function() {
        if (Main.overview.visible)
            Main.overview.hide();
        else
            Main.overview.showApps(); 
    },
};

class Extension {
    enable() {
        if (canDisconnect) {
            disconnectAllHandlers();
            // Customise the handler.
            global.display.connect('overlay-key', onOverlayKeyPressedCustom[gsVersion]);
        } else {
            this._handler = global.display.connect('overlay-key', () => {
                GLib.timeout_add(GLib.PRIORITY_DEFAULT, 500, () => {
                    Main.overview.viewSelector._showAppsButton.checked = true;
                    return GLib.SOURCE_REMOVE;
                });
            });
        }
    }

    disable() {
        if (canDisconnect) {
            disconnectAllHandlers();     
            // Retrieve the original function.   
            global.display.connect('overlay-key', onOverlayKeyPressedOriginal[gsVersion]);
        } else {
            this.disconnect(this._handler);
        }
    }
}

function init() {
    return new Extension();
}
