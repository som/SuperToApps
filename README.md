# Super to Apps

The extension maps the Super key to the apps view display.

## Install

1. Download and decompress or clone the repository
2. Place the resulting directory in `~/.local/share/gnome-shell/extensions`
3. **Change the directory name** to `super-to-apps@som.codeberg.org`
4. Xorg: type `alt + F2` and `r` to restart gnome-shell  
   Wayland: restart or re-login
5. Enable the extension with GNOME Extensions or GNOME Tweaks application
